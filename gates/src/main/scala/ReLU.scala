package com.drozdyuk.neuralnetworks.gates

import Math._

object ReLU {
  def apply() = new ReLU()
}

class ReLU() {
  var output = Wire.zero
  var input = Wire.zero

  def forward(input: Wire): Wire = {
    this.input = input
    output = Wire(max(0, input.value), 0.0f)
    output
  }

  def backward() = {
    // Set gradient to zero if neuron didn't fire
    if (input.value == 0) input.gradient = 0.0
    else input.gradient = output.gradient
  }
}
