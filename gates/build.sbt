// Multi-project sbt: http://www.scala-sbt.org/0.13/tutorial/Multi-Project.html

scalaVersion := "2.12.0-M1"

lazy val root = project.settings(
  name := "gates",
  description := "Implementation of simple gates and circuits as objects.")

// Project dependencies
//libraryDependencies += "com.typesafe.akka" % "akka-actor_2.11" % "2.4-M1"

// Test dependencies
libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test"
//libraryDependencies += "com.typesafe.akka" % "akka-testkit_2.11" % "2.4-M1" % "test"
