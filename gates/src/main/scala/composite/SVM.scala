package com.drozdyuk.neuralnetworks.gates
import Math._


object SVM {
  def apply() = new SVM()
}
class SVM() {
  /* SVM is just a particular type of a very simple circuit:
  Circuit that computes score = a*x + b*y + c where a,b,c are weights
  and x,y are data points.
  Source: http://karpathy.github.io/neuralnets/

  In other words, SVM is a circuit defined by inputs (x,y) and single output (score)!
  */

  // random initial parameter values
  val a = Wire(1.0, 0.0)
  val b = Wire(-2.0, 0.0)
  val c = Wire(-1.0, 0.0)

  // inputs
  var x = Wire.zero
  var y = Wire.zero

  // circuit
  val circuit = Circuit()

  var output = Wire.zero

  var by = Wire.zero
  var axpby = Wire.zero
  var axpbypc = Wire.zero

  // do forward pass
  def forward(x: Wire, y:Wire): Wire = {
    this.x = x
    this.y = y
    output = circuit.forward(x, y, a, b, c)
    output
  }

  def backward(label: Int) = { // lable is +1 or -1
    // Why ever do this???
    // Andriy: We need to do this constantly, otherwise
    // the "gradient" will always add to the previous iteration's
    // gradient, result in very high values of parameters.
    a.gradient = 0.0
    b.gradient = 0.0
    c.gradient = 0.0

    // comput pull based on what the output was
    var pull = 0.0
    if(label == 1 && output.value < 1){
      pull = 1.0f
    } else if(label == -1 && output.value > -1) {
      pull = -1.0f // score was too high
    }
    output.gradient = pull
    circuit.backward()

    // add regularization pull for parameters: towards zero and proportional to value
    a.gradient += -a.value * 0.01
    b.gradient += -b.value * 0.01
  }

  def parameterUpdate() = {
    val h = 0.01 // step size
    a.value += h * a.gradient
    b.value += h * b.gradient
    c.value += h * c.gradient
  }

  def learnFrom(xValue:Double, yValue:Double, label: Int) = {
    x.value = xValue
    y.value = yValue
    forward(x, y)
    backward(label)
    parameterUpdate()
  }

}
