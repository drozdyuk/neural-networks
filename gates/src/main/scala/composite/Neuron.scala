package com.drozdyuk.neuralnetworks.gates

import Math._

// Neuron that has input x,y,a,b,c and generates an output wire s every
// time forward is called.
// Applies sigmoid function to output.
object Neuron {
  def apply() = new Neuron()
}
class Neuron() {
  val circuit = Circuit()
  val sig = SigmoidGate()

  def forward(x: Wire, y: Wire, a: Wire, b: Wire, c: Wire): Wire = {
    sig.forward(circuit.forward(x, y, a, b, c))       // sig(ax + by + c)  == 0.8808
  }

  def backward() = {
    sig.backward()
    circuit.backward()
  }
}
