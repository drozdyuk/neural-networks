package com.drozdyuk.neuralnetworks.gates

import org.scalatest.WordSpecLike
import org.scalatest.Matchers
import org.scalatest.BeforeAndAfterAll
import com.drozdyuk.neuralnetworks.gates._
import scala.language.postfixOps
import scala.concurrent.duration._
import scala.math._
import scala.util._

class NeuralNetworkSpec extends WordSpecLike with Matchers {
    "Neural Network must train correctly" in {
      val data:List[(Double,Double)] = List(
        (1.2, 0.7),
        (-0.3, -0.5),
        (3.0, 0.1),
        (-0.1, -1.0),
        (-1.0, 1.1),
        (2.1, -3))
      val labels:List[Int] = List(1, -1, 1, -1, -1, 1)

      val x = Wire(0.0, 0.0)
      val y = Wire(0.0, 0.0)

      val nn = NeuralNetwork()

      def evalTrainingAccuracy():Double = {
        var num_correct = 0.0
        for((datum, label) <- data.zip(labels)) {
          x.value = datum._1
          y.value = datum._2
          val predicted = if(nn.forward(x, y).value > 0) 1 else -1
          if (predicted == label) num_correct += 1.0f
        }
        num_correct / (data.length * 1.0f)
      }

      // teach nn using stochastic gradient descent
      var numIterations = 0
      val maxIterations = 500
      var notTrained = true
      var iterationsExceeded = false

      while(notTrained && !iterationsExceeded) {
        // pick a random data point
        val i = floor(Random.nextDouble() * data.length).toInt
        // reset inputs
        x.value = data(i)._1
        x.gradient = 0.0f
        y.value = data(i)._2
        y.gradient = 0.0f
        val label = labels(i)
        nn.learnFrom(x.value, y.value, label)
        val accuracy  = evalTrainingAccuracy()
        if(accuracy == 1.0){
          notTrained = false
          println(s"NN reached 100% accuracy after $numIterations iterations.")
        }
        numIterations += 1
        if(numIterations > maxIterations) { iterationsExceeded = true }
      }
      // Our network should be 100% effective!
      evalTrainingAccuracy() should be (1.0 +- 0.0001)

    }


  }
