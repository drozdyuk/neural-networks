package com.drozdyuk.neuralnetworks.gates
import akka.actor._

object MultGate {
  def props(left:ActorRef, right:ActorRef, output:ActorRef) = Props(new MultGate(left, right, output))
}

class MultGate(left:ActorRef, right:ActorRef, output:ActorRef) extends Actor {
  // In the case left and right are the same sender,
  // sets inputs in order of received messages 
  var a:Option[Signal] = None
  var b:Option[Signal] = None

  def receive = forward

  def fire() = {
    output ! Signal(a.get.value * b.get.value, Math.max(a.get.id, b.get.id))
    context.become(backward)
  }

  def backpropagate(gradient: Gradient) = {
    left !  Gradient(b.get.value * gradient.value, gradient.id)
    right ! Gradient(a.get.value * gradient.value, gradient.id)
    // Reset inputs
    a = None
    b = None
    context.become(forward)
  }

  def forward: Receive = {
    case signal: Signal =>
      if(sender == left && a.isEmpty) {
        a = Some(signal)
      }
      else if(sender == right && b.isEmpty) {
        b = Some(signal)
      }
      if(!a.isEmpty && !b.isEmpty) {
        fire()
      }
  }

  def backward: Receive = {
    case gradient: Gradient if sender == output =>
      backpropagate(gradient)
  }
}
