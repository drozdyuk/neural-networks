package com.drozdyuk.neuralnetworks.gates

import Math._

// Linear classifier for 3 variables (x,y,z), of the form: ax + by + cz + d
object Circuit3 {
  def apply() = new Circuit3()
}
class Circuit3() {
  // create gates
  val mulg0 = MultGate()
  val mulg1 = MultGate()
  val mulg2 = MultGate()
  val addg0 = AddGate()
  val addg1 = AddGate()
  val addg2 = AddGate()

  // do forward pass
  def forward(x: Wire, y: Wire, z: Wire, a: Wire, b: Wire, c: Wire, d:Wire): Wire = {
    val ax = mulg0.forward(a,x)
    val by = mulg1.forward(b,y)
    val cz = mulg2.forward(c,z)
    val axby = addg0.forward(ax, by)
    val axbycz = addg1.forward(axby, cz)
    val output = addg2.forward(axbycz, d)
    output
  }

  def backward() = {
    addg2.backward()
    addg1.backward()
    addg0.backward()
    mulg2.backward()
    mulg1.backward()
    mulg0.backward()
  }
}
