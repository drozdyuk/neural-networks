package com.drozdyuk.neuralnetworks.gates

import org.scalatest.WordSpecLike
import org.scalatest.Matchers
import org.scalatest.BeforeAndAfterAll
import com.drozdyuk.neuralnetworks.gates._
import scala.language.postfixOps
import scala.concurrent.duration._
import scala.math._
import scala.util._

class GateSpecs extends WordSpecLike with Matchers {

    "Neuron must propagate forward and backward correctly" in {
      val a = Wire(1.0, 0.0)
      val b = Wire(2.0, 0.0)
      val c = Wire(-3.0, 0.0)
      val x = Wire(-1.0, 0.0)
      val y = Wire(3.0, 0.0)

      val neuron = Neuron()
      var s = neuron.forward(x, y, a, b, c)
      s.value should be (0.8808 +- 0.001)
      // set the gradient on output
      s.gradient = 1.0f
      neuron.backward()
      val h = 0.01f // step size

      a.gradient should be (-0.105 +- 0.001)
      b.gradient should be (0.315 +- 0.001)
      c.gradient should be (0.105 +- 0.001)
      x.gradient should be (0.105 +- 0.001)
      y.gradient should be (0.210 +- 0.001)

      a.value += h * a.gradient
      b.value += h * b.gradient
      c.value += h * c.gradient
      x.value += h * x.gradient
      y.value += h * y.gradient

      s = neuron.forward(x, y, a, b, c)
      s.value should be (0.8825 +- 0.001)

    }

    "AddGate must propagate forward and backward correctly" in {
      val add = AddGate()
      val x = Wire(2.0f, 0.0f)
      val y = Wire(1.0f, 0.0f)
      val output = add.forward(x, y)
      output.value should be (3.0f)
      output.gradient should be (0.0f)

      output.gradient = 1.0f
      add.backward()
      x.gradient should be (1.0f)
      y.gradient should be (1.0f)
    }

    "MultGate must propagate forward and backward correctly" in {
      val mult = MultGate()
      val x = Wire(2.0f, 0.0f)
      val y = Wire(3.0f, 0.0f)
      val output = mult.forward(x, y)
      output.value should be (6.0f)
      output.gradient should be (0.0f)

      output.gradient = 1.0f
      mult.backward()
      x.gradient should be (3.0f)
      y.gradient should be (2.0f)
    }

    "SigmoidGate must propagate forward and backward correctly" in {
      val sig = SigmoidGate()
      val x = Wire(2.0f, 0.0f)
      val output = sig.forward(x)
      output.value should be (0.88 +- 0.1)
      output.gradient should be (0.0f)

      output.gradient = 1.0f
      sig.backward()
      x.gradient should be ( 0.88 * (1 - 0.88) +- 0.1)
    }

    "ReLU gate must not propagate if it didn't fire" in {
      val relu = ReLU()
      val x = Wire(0.0f, 0.0f)
      val o1 = relu.forward(x)

      o1.value should be (0.0 +- 0.1)
      o1.gradient should be (0.0)
      o1.gradient = 1.0f

      relu.backward()
      x.gradient should be (0.0)

      // Now fire
      val y = Wire(1.0, 0.0)
      val o2 = relu.forward(y)
      o2.value should be (1.0)
      o2.gradient should be (0.0)
      o2.gradient = 1.1
      relu.backward()
      y.gradient should be(1.1)
      // make sure nothing fancy is going on:
      x.gradient should be (0.0)
    }


  }
