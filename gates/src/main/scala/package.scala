package com.drozdyuk.neuralnetworks.gates
import scala.util._
class Wire(var value: Double, var gradient: Double )

object Wire {
  def apply(value:Double, gradient:Double) = new Wire(value, gradient)
  def zero = new Wire(0.0, 0.0)
  def random = new Wire(Random.nextDouble() - 0.5, 0.0)
}
