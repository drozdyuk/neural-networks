/* Attempt to hack together few gates
case class Output(target:ActorRef)
case class Input(target:ActorRef)
class Wire extends Actor {
  var outpus = List[ActorRef].empty
  var inputs = List[ActorRef].empty

  def receive = {
    case Output(target) = outputs += target
    case Input(target) = inputs += target
  }
}
*/

/*class LinearClassifier(x_input: ActorRef, y_input: ActorRef, h:Double = 0.01) extends Actor {
  import Parameter._

  var x: Option[Signal] = None
  var y: Option[Signal] = None

  val a = context.actorOf(Parameter.props(h))
  val b = context.actorOf(Parameter.props(h))
  val c = context.actorOf(Parameter.props(h))

  val ax = context.actorOf(MultGate.props(left=a, right=x_input))
  val by = context.actorOf(MultGate.props(left=b, right=y_input))
  //  q = ax + by
  val q = context.actorOf(AddGate.props(left=ax, right=bx))
  //  z = q + c
  val z = context.actorOf(AddGate.props(left=q, right=c))

  x_input ! Output(ax)
  y_input ! Output(by)

  def waiting: Receive = {
    case Output(output) =>
      z ! Output(output)
      context.become(forward(output))
  }

  def input_ready = !x.isEmpty && !y.isEmpty

  def fire(output: ActorRef) = {
    // Fire parameters!
    a ! Forward
    b ! Forward
    c ! Forward
    context.become(backward(output))
  }

  def backward(output: ActorRef): Receive = {
    case gradient: Gradient if sender == output =>
        backpropagate(gradient, output)
  }

  def backpropagate(gradient: Gradient, output: ActorRef) = {
    q ! Gradient(1.0 * gradient.value, gradient.id)
    c ! Gradient(1.0 * gradient.value, gradient.id)
    // Reset inputs
    val x = None
    val y = None
    context.become(forward(output))
  }

  def forward(output:ActorRef): Receive = {
    case signal:Signal =>
      if(sender == x_input && x.isEmpty){
        x = Some(signal)
      } else if(sender == y_input && y.isEmpty) {
        y = Some(signal)
      }
      if(input_ready) fire(output)
  }

}*/
