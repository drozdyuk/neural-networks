package com.drozdyuk.neuralnetworks.gates

/* id - is the association id useful for tracing.
   Components should forward/backward propagate the id they receive.
   In case multiple inputs send differing ids - pick the max id.
 */
case class Signal(value:Double, id:Int)
case class Gradient(value:Double, id:Int)

import akka.actor._
/* Set the output explicitly */
case class Output(output: ActorRef)
