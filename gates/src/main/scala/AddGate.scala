package com.drozdyuk.neuralnetworks.gates


object AddGate {
  def apply() = new AddGate()
}
class AddGate {
    var w1 = Wire.zero
    var w2 = Wire.zero
    var output = Wire.zero

    def forward(w1: Wire, w2: Wire): Wire = {
        this.w1 = w1
        this.w2 = w2
        output = Wire(w1.value + w2.value, 0.0f)
        output
    }

    def backward() = {
        w1.gradient += 1.0f * output.gradient
        w2.gradient += 1.0f * output.gradient
    }
}
