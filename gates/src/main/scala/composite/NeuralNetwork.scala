package com.drozdyuk.neuralnetworks.gates
import Math._
import scala.util._

object NeuralNetwork {
  def apply() = new NeuralNetwork()
}

class NeuralNetwork() {
  /* Generalized SVM into a 2-layer Neural network.
  SVM is just a particular type of a very simple circuit:
  Circuit that computes score = a*x + b*y + c where a,b,c are weights
  and x,y are data points.
  Source: http://karpathy.github.io/neuralnets/

  In other words, SVM is a circuit defined by inputs (x,y) and single output (score)!
  */

  val h = 0.01 // step size

  // inputs
  var x = Wire.zero
  var y = Wire.zero

  // random initial parameter values
  val a1, a2, a3 = Wire.random
  val b1, b2, b3 = Wire.random
  val c1, c2, c3 = Wire.random

  // neurons - connect into: k1*n1 + k2*n2 + k3*n3 + k4
  val n1 = ReLUCircuit()
  val n2 = ReLUCircuit()
  val n3 = ReLUCircuit()
  val n4 = Circuit3()

  val k1, k2, k3, k4 = Wire.random
  var output = Wire.zero

  // do forward pass
  def forward(x: Wire, y: Wire): Wire = {
    // Reset x and y from input:
    this.x = x
    this.y = y

    val o1 = n1.forward(x, y, a1, b1, c1)
    val o2 = n2.forward(x, y, a2, b2, c2)
    val o3 = n3.forward(x, y, a3, b3, c3)

    // final combining neuron
    output = n4.forward(o1, o2, o3, k1, k2, k3, k4)
    output
  }


  def backward(label: Int) = { // lable is +1 or -1
    //// reset pulls
    // Andriy: We need to do this constantly, otherwise
    // the "gradient" will always add to the previous iteration's
    // gradient, result in very high values of parameters.
    reset(List(a1, a2, a3, b1, b2, b3, c1, c2, c3, k1, k2, k3, k4))

    // comput pull based on what the output was
    var pull = 0.0
    if(label == 1 && output.value < 1){
      pull = 1.0f
    } else if(label == -1 && output.value > -1) {
      pull = -1.0f // score was too high
    }
    output.gradient = pull
    n4.backward()
    n3.backward()
    n2.backward()
    n1.backward()

    // add regularization pull for parameters: towards zero and proportional to value
    // not on: c_i or k4.
    regularize(List(a1, a2, a3, b1, b2, b3, k1, k2, k3))
  }

  def regularize(ws:List[Wire]) = {
    for(w <- ws) {
      w.gradient += -w.value * h
    }
  }
  def update(ws:List[Wire]) {
    for(w <- ws) {
      w.value += h * w.gradient
    }
  }
  def reset(ws:List[Wire]) = {
    for(w <- ws) w.gradient = 0.0
  }

  def parameterUpdate() = {
    update(List(a1, a2, a3, b1, b2, b3, c1, c2, c3, k1, k2, k3, k4))
  }

  def learnFrom(xValue:Double, yValue:Double, label: Int) = {
    x.value = xValue
    y.value = yValue
    forward(x, y)
    backward(label)
    parameterUpdate()
  }

}
