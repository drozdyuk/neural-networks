package com.drozdyuk.neuralnetworks.gates

import akka.actor.ActorSystem
import akka.actor.Actor
import akka.actor.Props
import akka.testkit.{TestKit, ImplicitSender, TestActorRef, TestProbe}
import org.scalatest.WordSpecLike
import org.scalatest.Matchers
import org.scalatest.BeforeAndAfterAll
import com.drozdyuk.neuralnetworks.gates._
import scala.language.postfixOps
import scala.concurrent.duration._

class AddGateSpec(_system: ActorSystem) extends TestKit(_system) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll {
    def this() = this(ActorSystem("AddGateSpec"))

    override def afterAll {
      TestKit.shutdownActorSystem(system)
    }

    "AddGate must trigger output correctly" in {
      val gate = TestActorRef(AddGate.props(testActor, testActor, testActor))

      gate ! Signal(value=5.0f, id=1)
      gate ! Signal(value=4.0f, id=1)
      // output signal
      expectMsg(Signal(9.0f, 1))
    }

    "AddGate must back-propagate correctly to each input" in {
      val left = TestProbe()
      val right = TestProbe()
      val gate = TestActorRef(AddGate.props(left.ref, right.ref, testActor))
      // Set the gate into ready state for back-propagation
      gate.tell(Signal(value=1.0f, id=1), left.ref)
      gate.tell(Signal(value=1.0f, id=1), right.ref)
      expectMsgType[Signal]

      // Backpropagate
      gate.tell(Gradient(2.0f, id=2), testActor)
      left.expectMsg(Gradient(2.0f, id=2))
      right.expectMsg(Gradient(2.0f, id=2))
    }

    "AddGate must not fire after back-propagation on single input" in {
      val left = TestProbe()
      val right = TestProbe()
      val gate = TestActorRef(AddGate.props(testActor, testActor, testActor))
      // Set the gate into ready state for back-propagation
      gate ! Signal(value=1.0f, id=1)
      gate ! Signal(value=1.0f, id=1)
      expectMsgType[Signal]
      // Backpropagate
      gate ! Gradient(1.0f, id=2)
      expectMsgType[Gradient]
      expectMsgType[Gradient]
      // Now send only one signal:
      gate ! Signal(value=2.0f, id=1)
      expectNoMsg(100 milliseconds)
      // now send anothr
      gate ! Signal(value=2.0f, id=1)
      expectMsgType[Signal]
    }
  }
