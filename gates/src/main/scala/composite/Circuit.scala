package com.drozdyuk.neuralnetworks.gates

import Math._

// Simple linear classifier circuit, that is almost the same as simple neuron,
// but leaves out the sigmoid function.
object Circuit {
  def apply() = new Circuit()
}
class Circuit() {
  // create gates
  val mulg0 = MultGate()
  val mulg1 = MultGate()
  val addg0 = AddGate()
  val addg1 = AddGate()

  // do forward pass
  def forward(x: Wire, y: Wire, a: Wire, b: Wire, c: Wire): Wire = {
    val ax = mulg0.forward(a,x) // a*x    == - 1
    val by = mulg1.forward(b,y) // b*y    == 6
    val axpby = addg0.forward(ax, by) // a*x + b*y == 2
    addg1.forward(axpby, c) // a*x + b*y + c == 2
  }

  def backward() = {
    addg1.backward()
    addg0.backward()
    mulg1.backward()
    mulg0.backward()
  }
}
