package com.drozdyuk.neuralnetworks.gates
import akka.actor._

object Parameter {
  case object Forward
  case class Value(value: Double)

  def props(client: ActorRef, value: Double, h:Double) = Props(new Parameter(client, value, h))
}

class Parameter(client: ActorRef, var value: Double, h:Double) extends Actor {
  // Actor that is just responsible for storing and updating parameter value
  import Parameter._

  def receive = waitingForOutput

  def waitingForOutput: Receive = {
    case Output(output: ActorRef) =>
      context.become(unlocked(output))
  }

  def unlocked(output: ActorRef): Receive = {
    // Send the signal forward
    case Forward =>
      output ! Signal(value, 0)
      context.become(locked(output))
  }
  def locked(output: ActorRef): Receive = {
    case Gradient(g, id) =>
      value += g * h
      // Send our value to parent
      client ! Value(value)
      context.become(unlocked(output))
  }
}
