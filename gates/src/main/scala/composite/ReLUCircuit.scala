package com.drozdyuk.neuralnetworks.gates

import Math._

// Puts a ReLU after the circuit output
object ReLUCircuit {
  def apply() = new ReLUCircuit()
}
class ReLUCircuit() {
  val circuit = Circuit()
  val relu = ReLU()

  // do forward pass
  def forward(x: Wire, y: Wire, a: Wire, b: Wire, c: Wire): Wire = {
    relu.forward(circuit.forward(x, y, a, b, c))
  }

  def backward() = {
    relu.backward()
    circuit.backward()
  }
}
