package com.drozdyuk.neuralnetworks.gates

import Math._

object SigmoidGate {
  def apply() = new SigmoidGate()
}

class SigmoidGate {
  var input = Wire.zero
  var output = Wire.zero

  def sig(x:Double) = 1 / ( 1 + exp(-x) )

  def forward(w: Wire): Wire = {
    input = w
    output = Wire(sig(input.value), 0.0f)
    output
  }

  def backward() = {
    val s = sig(input.value)
    input.gradient += (s * (1.0f - s)) * output.gradient
  }
}
