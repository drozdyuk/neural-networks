package com.drozdyuk.neuralnetworks.gates

import akka.actor.ActorSystem
import akka.actor.Actor
import akka.actor.Props
import akka.testkit.{TestKit, ImplicitSender, TestActorRef, TestProbe}
import org.scalatest.WordSpecLike
import org.scalatest.Matchers
import org.scalatest.BeforeAndAfterAll
import com.drozdyuk.neuralnetworks.gates._
import scala.language.postfixOps
import scala.concurrent.duration._

class AddSpec(_system: ActorSystem) extends TestKit(_system) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll {
    def this() = this(ActorSystem("AddSpec"))

    override def afterAll {
      TestKit.shutdownActorSystem(system)
    }

    "AddGate must trigger output correctly" in {
      val gate = TestActorRef(Add.props(testActor, testActor))
      val testProbe = TestProbe()
      gate ! Output(testProbe.ref)

      gate ! Signal(value=5.0f, id=1)
      gate ! Signal(value=4.0f, id=1)
      // output signal
      testProbe.expectMsg(Signal(9.0f, 1))
    }

    "AddGate must backpropage to parameter correctly" in {
      import Parameter._
      val h = 0.1

      // a + b
      val a = TestActorRef(Parameter.props(testActor, 1.0, h))
      val b = TestActorRef(Parameter.props(testActor, 2.0, h))

      val gate = TestActorRef(Add.props(b, a))
      expectMsgType[Output]
      expectMsgType[Output]

      gate ! Output(testActor)
      //expectMsg(Output(gate))

      // Fire all params
      a ! Forward
      b ! Forward
      // output signal
      expectMsg(Signal(3.0f, id=0))

      gate.tell(Gradient(1.0f, id=2), testActor)

      expectMsgType[Value]
      expectMsgType[Value]

      // Fire all params
      a ! Forward
      b ! Forward
      expectMsgType[Signal]
      gate.tell(Gradient(1.0f, id=3), testActor)
      expectMsgType[Value]
      expectMsgType[Value]
    }

    "AddGate must set outpus and back-propagate correctly to each input" in {
      val left = TestProbe()
      val right = TestProbe()
      val gate = TestActorRef(Add.props(left.ref, right.ref))
      gate ! Output(testActor)

      left.expectMsg(Output(gate))
      right.expectMsg(Output(gate))

      // Set the gate into ready state for back-propagation
      gate.tell(Signal(value=1.0f, id=1), left.ref)
      gate.tell(Signal(value=1.0f, id=1), right.ref)
      expectMsgType[Signal]

      // Backpropagate
      gate.tell(Gradient(2.0f, id=2), testActor)
      left.expectMsg(Gradient(2.0f, id=2))
      right.expectMsg(Gradient(2.0f, id=2))
    }

    "AddGate must not fire after back-propagation on single input" in {
      val left = TestProbe()
      val right = TestProbe()
      val gate = TestActorRef(Add.props(left.ref, right.ref))

      left.expectMsg(Output(gate))
      right.expectMsg(Output(gate))

      gate ! Output(testActor)

      // Set the gate into ready state for back-propagation
      gate.tell(Signal(value=1.0f, id=1), left.ref)
      gate.tell(Signal(value=1.0f, id=1), right.ref)

      expectMsgType[Signal]

      // Backpropagate
      gate ! Gradient(1.0f, id=2)
      left.expectMsgType[Gradient]
      right.expectMsgType[Gradient]
      // Now send only one signal:
      gate.tell(Signal(value=2.0f, id=1), left.ref)
      expectNoMsg(100 milliseconds)
      // now send anothr
      gate.tell(Signal(value=2.0f, id=1), right.ref)
      expectMsgType[Signal]
    }
  }
