package com.drozdyuk.neuralnetworks.gates
import akka.actor._

object AddGate {
  def props(left:ActorRef, right:ActorRef, output:ActorRef) = Props(new AddGate(left, right, output))
}

class AddGate(left:ActorRef, right:ActorRef, output:ActorRef) extends Actor {
    var a:Option[Signal] = None
    var b:Option[Signal] = None

    def receive = forward

    def fire() = {
      output ! Signal(a.get.value + b.get.value, Math.max(a.get.id, b.get.id))
      context.become(backward)
    }

    def backpropagate(gradient:Gradient) = {
      left  ! Gradient(gradient.value * 1.0f, gradient.id)
      right ! Gradient(gradient.value * 1.0f, gradient.id)
      // Reset inputs
      a = None
      b = None
      context.become(forward)
    }

    def forward: Receive = {
      case signal:Signal =>
        if(sender == left && a.isEmpty) {
          a = Some(signal)
        } else if(sender == right && b.isEmpty) {
          b = Some(signal)
        }
        if(!a.isEmpty && !b.isEmpty) fire()
        // Else ignore signal.
    }
    def backward: Receive = {
      case gradient:Gradient if sender == output =>
        backpropagate(gradient)
    }
}
