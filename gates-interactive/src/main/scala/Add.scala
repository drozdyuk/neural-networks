package com.drozdyuk.neuralnetworks.gates
import akka.actor._

object Add {
  def props(left:ActorRef, right:ActorRef) = Props(new Add(left, right))
}

class Add(left:ActorRef, right:ActorRef) extends Actor {
    // Let inputs know about us
    left ! Output(self)
    right ! Output(self)

    var a:Option[Signal] = None
    var b:Option[Signal] = None

    def receive = waiting

    def waiting: Receive = {
      case Output(output) => context.become(forward(output))
    }


    def forward(output: ActorRef): Receive = {
      case signal:Signal =>
        if(sender == left && a.isEmpty) {
          a = Some(signal)
        } else if(sender == right && b.isEmpty) {
          b = Some(signal)
        } else {
          println(s"Ignoring signal from unknown sender: $sender")
        }
        if(!a.isEmpty && !b.isEmpty) fire(output)
        // Else ignore signal.
    }

    def fire(output: ActorRef) = {
      output ! Signal(a.get.value + b.get.value, Math.max(a.get.id, b.get.id))
      context.become(backward(output))
    }

    def backward(output: ActorRef): Receive = {
      case gradient:Gradient if sender == output =>
        backpropagate(gradient, output)
      case Gradient => println(s"Ignoring gradient from unknown sender: $sender")
    }

    def backpropagate(gradient:Gradient, output: ActorRef) = {
      left  ! Gradient(gradient.value * 1.0f, gradient.id)
      right ! Gradient(gradient.value * 1.0f, gradient.id)
      // Reset inputs
      a = None
      b = None
      context.become(forward(output))
    }
}
